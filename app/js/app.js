   "use strict";
   
   var TodoModel = function () {
     this.model = [];
   };

   TodoModel.prototype.uid = function () {
     return Math.round(Math.random() * 100000000);
   };

   TodoModel.prototype.add = function (task) {
     var newTask;

     if (task.trim() !== "") {
       newTask = {
         "_id": this.uid(),
         "task": task.trim(),
         "done": false,
         "regdate": (new Date()).getTime()
       };
       this.model.push(newTask);
       this.notify(newTask);
       
       console.log('todo:list', this.model);
     }
     else {
       alert('할 일을 입력해주세요');
     }
   };

   TodoModel.prototype.notify = function (newTask) {
     var event = new CustomEvent("todo:added", {
       'detail': newTask
     });
     document.dispatchEvent(event);
   };

   // Todo Controller 
   var TodoCtrl = function () {
     this.tplTaskList = this.getTemplate('tpl-task-list');

     this.onAdded();
   };

   TodoCtrl.prototype.getTemplate = function (tid) {
     var el = document.getElementById(tid);
     return {
       tpl: el.innerText,
       wrapTag: el.dataset.wrapTag
     }
   };

   TodoCtrl.prototype.onAdded = function () {
     var self = this;

     document.addEventListener('todo:added', function (e) {
       var newTask = e.detail,
         tpl = self.tplTaskList.tpl,
         wrapTag = self.tplTaskList.wrapTag,
         node,
         el;

       console.log('todo:added:', newTask);
       tpl = tpl.replace(/{{task}}/, newTask.task);
       node = document.createDocumentFragment();
       el = document.createElement(wrapTag)
       el.id = 'task-' + newTask._id;
       el.innerHTML = tpl;
       node.appendChild(el);

       document.getElementById('active-list').appendChild(node);
     });
   };

   var todos = new TodoModel(),
     todoCtrl = new TodoCtrl();